#!/bin/bash

  # 1. compile

  g++  -O3                               \
       -Wall                             \
       -std=gnu++17                      \
       -static                           \
       driver_program.cpp                \
       /usr/lib/x86_64-linux-gnu/libgsl.a \
       -o x_gnu
