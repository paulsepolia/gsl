//==================================//
// Function: gsl_wavelet_daubechies //
//==================================//

// keywords: gsl_sort, gsl_wavelet, daubechies

#include <iostream>
#include <cstdio>
#include <cmath>
#include <iomanip>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_wavelet.h>

using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::showpos;
using std::fixed;
using std::showpoint;

// the main function

int main()
{
    // local variables and parameters

    cout << "  1 --> space allocation" << endl;

    const int N_DIM = static_cast<int>(pow(2, 27));
    const int nc = static_cast<int>(pow(2, 10));
    const int ndisp = 5;
    double * data = new double [N_DIM];
    double * abscoeff =  new double [N_DIM];
    size_t * p = new size_t [N_DIM];

    // output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(20);

    //

    gsl_wavelet * w;

    //

    gsl_wavelet_workspace * work;

    //

    w = gsl_wavelet_alloc(gsl_wavelet_daubechies, 4);

    //

    cout << "  2 --> work space allocation" << endl;

    work = gsl_wavelet_workspace_alloc(N_DIM);

    //

    cout << "  3 --> build the data set" << endl;

    for (int i = 0; i < N_DIM; i++) {
        data[i] = i;
    }

    //

    cout << "  4 --> wavelet transform execution" << endl;

    gsl_wavelet_transform_forward(w, data, 1, N_DIM, work);

    //

    cout << " --> data[0] = " << data[0] << endl;
    cout << " --> data[1] = " << data[1] << endl;

    //

    cout << "  5 --> abscoeff[i] = fabs(data[i])" << endl;

    for (int i = 0; i < N_DIM; i++) {
        abscoeff[i] = fabs(data[i]);
    }

    //

    cout << "  6 --> gsl_sort_index" << endl;

    gsl_sort_index(p, abscoeff, 1, N_DIM);

    cout << " --> abscoeff[p[N_DIM-2]]  = " << abscoeff[p[N_DIM-2]]  << endl;
    cout << " --> abscoeff[p[N_DIM-1]]  = " << abscoeff[p[N_DIM-1]]  << endl;

    //

    cout << "  7 --> setting wavelet data to zero" << endl;

    for (int i = 0; (i + nc) < N_DIM; i++) {
        data[p[i]] = 0;
    }

    //

    cout << "  8 --> wavelet reverse transform" << endl;

    gsl_wavelet_transform_inverse(w, data, 1, N_DIM, work);

    //

    cout << "  9 --> display results" << endl;

    for (int i = 0; i < ndisp; i++) {
        cout <<  " --> " << i << " --> " << data[i] << endl;
    }

    for (int i = N_DIM - ndisp; i < N_DIM; i++) {
        cout <<  " --> " << i << " --> " << data[i] << endl;
    }

    //

    cout << " 10 --> free up RAM for wavelet" << endl;

    gsl_wavelet_free(w);

    //

    cout << " 11 --> free up RAM for work" << endl;

    gsl_wavelet_workspace_free(work);

    //

    cout << " 12 --> delete data" << endl;

    delete [] data;

    //

    cout << " 13 --> delete abscoeff" << endl;

    delete [] abscoeff;

    //

    cout << " 14 --> delete p" << endl;

    delete [] p;

    //

    // sentineling

    cout << " 15 --> end" << endl;

    int sentinel;
    cin >> sentinel;

    cout << " 16 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

