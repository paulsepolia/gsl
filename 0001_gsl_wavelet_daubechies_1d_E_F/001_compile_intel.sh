#!/bin/bash

  # 1. compile

  icpc -O3                               \
       -xHost                            \
       -Wall                             \
       -std=gnu++17                      \
       -static                           \
       -wd2012                           \
       driver_program.cpp                \
       /usr/lib/x86_64-linux-gnu/libgsl.a \
       -o x_intel
