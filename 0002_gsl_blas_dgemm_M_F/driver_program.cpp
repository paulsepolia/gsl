//==========================//
// Function: gsl_blas_dgemm //
//==========================//

// keywords: gsl_blas_dgemm, gsl_matrix_view

#include <iostream>
#include <cstdio>
#include <gsl/gsl_blas.h>

using std::cout;
using std::cin;
using std::endl;

// the main function

int main()
{
    // matrix initialization

    double a[] = { 0.11, 0.12, 0.13,
                   0.21, 0.22, 0.23
                 };

    double b[] = { 1011, 1012,
                   1021, 1022,
                   1031, 1032
                 };

    double c[] = { 0.00, 0.00,
                   0.00, 0.00
                 };

    // set vectors to matrix view

    gsl_matrix_view  A = gsl_matrix_view_array(a, 2, 3);
    gsl_matrix_view  B = gsl_matrix_view_array(b, 3, 2);
    gsl_matrix_view  C = gsl_matrix_view_array(c, 2, 2);

    // compute C = A B

    gsl_blas_dgemm(CblasNoTrans,
                   CblasNoTrans,
                   1.0,
                   &A.matrix,
                   &B.matrix,
                   0.0,
                   &C.matrix);

    cout << " --> c[0] = " << c[0] << endl;
    cout << " --> c[1] = " << c[1] << endl;
    cout << " --> c[2] = " << c[2] << endl;
    cout << " --> c[3] = " << c[3] << endl;

    // sentineling

    cout << " --> end" << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

