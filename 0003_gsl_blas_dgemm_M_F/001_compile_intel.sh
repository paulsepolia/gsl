#!/bin/bash

  # 1. compile

  icpc -O3                                \
       -xHost                             \
       -Wall                              \
       -std=gnu++17                       \
       -wd2012                            \
       -qopenmp                           \
       driver_program.cpp                 \
       /usr/lib/x86_64-linux-gnu/libgsl.a \
       -mkl=parallel                      \
       -o x_intel
