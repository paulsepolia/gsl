
//==========================//
// Function: gsl_blas_dgemm //
//==========================//

// keywords: gsl_blas_dgemm, gsl_matrix_view

#include <iostream>
#include <cstdio>
#include <cmath>
#include <iomanip>

#include <gsl/gsl_blas.h>

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::setprecision;

// the main function

int main()
{
    // local variables and parameters

    const int DIM1 = 5.0 * static_cast<int>(pow(10.0, 3.0));
    const int DIM2 = 5.0 * static_cast<int>(pow(10.0, 3.0));
    const long TOT_ELEM = static_cast<long>(DIM1) * static_cast<long>(DIM2);
    const int K_MAX = 1000;
    long i;

    // adjust the output format

    cout << fixed;
    cout << setprecision(10);
    cout << showpos;
    cout << showpoint;

    // main test loop

    for (int k = 0; k != K_MAX; ++k) {
        cout << "------------------------------------------------------>> " << k << endl;

        // matrix declaration as vectors

        cout << " -->  1 --> declare matrices as vectors" << endl;

        double * a = new double [TOT_ELEM];
        double * b = new double [TOT_ELEM];
        double * c = new double [TOT_ELEM];

        // build matrices

        cout << " -->  2 --> build matrices as vectors" << endl;

        #pragma omp parallel default(none) \
        shared(a)     \
        shared(b)     \
        shared(c)     \
        private(i)
        {
            #pragma omp for

            for (i = 0; i < TOT_ELEM; ++i) {
                a[i] = cos(static_cast<double>(i));
                b[i] = sin(static_cast<double>(i));
                c[i] = 0.0;
            }
        }

        // set vectors to matrix view

        cout << " -->  3 --> set vectors to gsl matrix view" << endl;

        gsl_matrix_view  A = gsl_matrix_view_array(a, DIM1, DIM2);
        gsl_matrix_view  B = gsl_matrix_view_array(b, DIM2, DIM1);
        gsl_matrix_view  C = gsl_matrix_view_array(c, DIM1, DIM1);

        // compute C = dot(A,B)

        cout << " -->  4 --> execute the dot matrix product" << endl;

        gsl_blas_dgemm(CblasNoTrans,
                       CblasNoTrans,
                       1.0,
                       &A.matrix,
                       &B.matrix,
                       0.0,
                       &C.matrix);

        // some outputs

        cout << " -->  5 --> some outputs" << endl;

        cout << " -->  c[0] = " << c[0] << endl;
        cout << " -->  c[1] = " << c[1] << endl;
        cout << " -->  c[2] = " << c[2] << endl;
        cout << " -->  c[3] = " << c[3] << endl;

        // delete the heap space

        cout << " -->  6 --> delete heap allocated space" << endl;

        delete [] a;
        delete [] b;
        delete [] c;

    }

    // sentineling

    cout << " --> end" << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

